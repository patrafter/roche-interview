Roche interview demo project
-----------------

This is a demo application for the purpose of interview at Roche.
This app is a Spring Boot application running on default port 8080 and exposes REST API for managing products and orders. 

Find out more about available resources below.

## Build
To build the application type `./gradlew build`

## Test
To run unit tests type `./gradlew test`

## Run
To run the application type `./gradlew bootRun`

## API docs
Once you run the app you can find documentation of the API at http://localhost:8080/docs/index.html in the form of Ascii doc.

## Dockerize
To create Docker image from the project dir run: `docker build . -t roche-interview/demo-app`

## Running application along with Postgres DB with docker-compose
`docker-compose -f docker/docker-compose.yml up`

