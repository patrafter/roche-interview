package com.roche.interview.orders.application;

import com.roche.interview.orders.domain.LineItem;
import com.roche.interview.orders.domain.Order;
import com.roche.interview.orders.domain.OrderRepository;
import com.roche.interview.orders.domain.Product;
import com.roche.interview.sharedkernel.money.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class OrderServiceTest {
    OrderService service;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    TestEntityManager em;

    @BeforeEach
    void setUp() {
        service = new OrderService(orderRepository);
    }

    @Test
    void shouldPlaceOrder() {
        //given

        List<LineItem> lineItems = List.of(
                LineItem.ofProduct(createProduct("product 1"), 1),
                LineItem.ofProduct(createProduct("product 2"), 2)
        );

        //when
        service.placeOrder("buyer@roche.com", lineItems);

        //then
        List<Order> savedOrders = em.getEntityManager().createQuery("SELECT o FROM Order o", Order.class).getResultList();
        assertThat(savedOrders).hasSize(1);

        Order savedOrder = savedOrders.get(0);
        List<LineItem> savedLineItems = savedOrder.getLineItems();

        assertThat(savedOrder.getBuyerEmail()).isEqualTo("buyer@roche.com");
        assertThat(savedOrder.getDatetime().toLocalDate()).isEqualTo(LocalDate.now());
        assertThat(savedOrder.getTotalPrice().getAmount()).isEqualTo(BigDecimal.valueOf(3));
        assertThat(savedLineItems).hasSize(2);
        assertThat(savedLineItems.get(0).getQuantity()).isEqualTo(1);
        assertThat(savedLineItems.get(0).getProduct().getName()).isEqualTo("product 1");
        assertThat(savedLineItems.get(1).getQuantity()).isEqualTo(2);
        assertThat(savedLineItems.get(1).getProduct().getName()).isEqualTo("product 2");
    }

    @Test
    void shouldRetrieveOrdersInPeriod() {
        //given
        Order newOrder = service.placeOrder("buyer@roche.com", List.of(LineItem.ofProduct(createProduct("product 1"), 1)));
        Order oldOrder = service.placeOrder("buyer@roche.com", List.of(LineItem.ofProduct(createProduct("product 2"), 1)));
        oldOrder.setDatetime(LocalDateTime.now().minusDays(1));
        em.persistAndFlush(oldOrder);

        //when
        List<Order> ordersFound = service.searchOrders(LocalDateTime.now().minusDays(2), LocalDateTime.now().minusDays(1));

        //then
        assertThat(ordersFound).hasSize(1);
        assertThat(ordersFound.get(0).getId()).isEqualTo(oldOrder.getId());
    }

    private Product createProduct(String productName) {
        return em.persistAndFlush(Product.create("sku " + productName, productName, new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));
    }
}