package com.roche.interview.orders.application;

import com.roche.interview.orders.domain.Product;
import com.roche.interview.orders.domain.ProductRepository;
import com.roche.interview.sharedkernel.money.Price;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class ProductServiceTest {
    ProductService service;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    TestEntityManager em;

    @BeforeEach
    void setUp() {
        service = new ProductService(productRepository);
    }


    @Test
    void shouldSaveProduct() {
        //given
        Product product = Product.create("sku1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN")));
        //when
        service.save(product);
        //then
        Optional<Product> savedProduct = productRepository.findById(product.getId());
        assertThat(savedProduct).isNotEmpty();
        assertThat(savedProduct.get().getSku()).isEqualTo("sku1");
        assertThat(savedProduct.get().getName()).isEqualTo("product 1");
        assertThat(savedProduct.get().getPrice()).isNotNull();
        assertThat(savedProduct.get().getPrice().getCurrency()).isEqualTo(Currency.getInstance("PLN"));
        assertThat(savedProduct.get().getPrice().getAmount()).isEqualTo(BigDecimal.ONE);
    }

    @Test
    void shouldFindAllProducts() {
        //given
        productRepository.save(Product.create("sku1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));
        productRepository.save(Product.create("sku2", "product 2", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));

        //when
        Iterable<Product> products = service.findAll();

        //then
        assertThat(products).hasSize(2);
    }


    @Test
    void shouldUpdateProduct() {
        //given
        Product product = em.persistAndFlush(Product.create("sku1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));
        em.detach(product);

        //when
        service.update(product.getId(), Product.create("sku 1-updated", "product 1-updated", new Price(BigDecimal.TEN, Currency.getInstance("PLN"))));

        //then
        Optional<Product> updatedProduct = productRepository.findById(product.getId());
        assertThat(updatedProduct).isPresent();
        assertThat(updatedProduct.get().getSku()).isEqualTo("sku 1-updated");
        assertThat(updatedProduct.get().getName()).isEqualTo("product 1-updated");
        assertThat(updatedProduct.get().getPrice().getAmount()).isEqualTo(BigDecimal.TEN);
        assertThat(updatedProduct.get().getPrice().getCurrency()).isEqualTo(Currency.getInstance("PLN"));
    }

    @Test
    void shouldSoftDeleteProduct() {
        //given
        Product product = productRepository.save(Product.create("sku1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));

        //when
        service.deleteById(product.getId());

        //then
        assertThat(productRepository.findById(product.getId())).isEmpty();
        Object deletedProduct = em.getEntityManager()
                .createNativeQuery("SELECT * FROM ipr_product WHERE id = " + product.getId(), Product.class)
                .getSingleResult();
        assertThat(deletedProduct).isNotNull();
    }
}