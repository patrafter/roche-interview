package com.roche.interview.orders.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roche.interview.orders.application.dto.ProductDto;
import com.roche.interview.orders.domain.Product;
import com.roche.interview.orders.domain.ProductRepository;
import com.roche.interview.sharedkernel.money.Price;
import com.roche.interview.sharedkernel.money.application.dto.PriceDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Currency;

import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = MOCK)
@AutoConfigureMockMvc
@AutoConfigureRestDocs
@Transactional
class ProductControllerTest {
    @Autowired
    MockMvc mockMvc;
    @Autowired
    ProductRepository productRepository;

    @Test
    public void getAllProducts() throws Exception {
        //given
        productRepository.save(Product.create("sku 1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));

        //when
        mockMvc.perform(get("/api/products"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].sku", is("sku 1")))
                .andExpect(jsonPath("$[0].name", is("product 1")))
                .andExpect(jsonPath("$[0].price.amount", is(1.00)))
                .andExpect(jsonPath("$[0].price.currency", is("PLN")))
                .andDo(document("products/all"));
    }

    @Test
    public void createProduct() throws Exception {
        //given
        ProductDto productDto = new ProductDto("sku 1", "product 1", new PriceDto(BigDecimal.TEN, "PLN"));

        //when
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(productDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(document("products/create"));
    }

    @Test
    public void updateProduct() throws Exception {
        //given
        Product existingProduct = productRepository.save(Product.create("sku 1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));
        ProductDto productDto = new ProductDto("sku 1 - changed", "product 1 - changed", new PriceDto(BigDecimal.TEN, "CHF"));

        //when
        mockMvc.perform(post("/api/products/{id}", existingProduct.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(productDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(document("products/update"));
    }

    @Test
    public void deleteProduct() throws Exception {
        //given
        Product existingProduct = productRepository.save(Product.create("sku 1", "product 1", new Price(BigDecimal.ONE, Currency.getInstance("PLN"))));

        //when
        mockMvc.perform(delete("/api/products/{id}", existingProduct.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(document("products/delete"));
    }

    private String asJsonString(Object o) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(o);
    }
}