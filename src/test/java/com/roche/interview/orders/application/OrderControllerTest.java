package com.roche.interview.orders.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.roche.interview.orders.application.dto.LineItemRequestDto;
import com.roche.interview.orders.application.dto.OrderRequestDto;
import com.roche.interview.orders.domain.LineItem;
import com.roche.interview.orders.domain.Order;
import com.roche.interview.orders.domain.Product;
import com.roche.interview.sharedkernel.money.Price;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = MOCK)
@AutoConfigureMockMvc
@AutoConfigureRestDocs
@Transactional
class OrderControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProductService productService;
    @Autowired
    OrderService orderService;

    @Test
    void searchOrders() throws Exception {
        //given
        Product product = productService.save(Product.create("sku 1", "product 1", new Price(BigDecimal.TEN, Currency.getInstance("PLN"))));
        Order order = orderService.placeOrder("buyer@roche.com", List.of(LineItem.ofProduct(product, 1)));

        //when
        mockMvc.perform(get("/api/orders/search?from={from}&to={to}", LocalDateTime.now().minusDays(1), LocalDateTime.now()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].orderId", is(order.getOrderId().toString())))
                .andExpect(jsonPath("$[0].buyerEmail", is("buyer@roche.com")))
                .andExpect(jsonPath("$[0].totalPrice.amount", is(10.00)))
                .andExpect(jsonPath("$[0].totalPrice.currency", is("PLN")))
                .andDo(document("orders/search"));
    }

    @Test
    void placeOrder() throws Exception {
        //given
        Product product = productService.save(Product.create("sku 1", "product 1", new Price(BigDecimal.TEN, Currency.getInstance("PLN"))));
        OrderRequestDto orderDto = new OrderRequestDto("buyer@roche.com", List.of(new LineItemRequestDto(1, product.getId())));

        //when
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(orderDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andDo(document("orders/place"));
    }

    private String asJsonString(Object o) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(o);
    }
}