package com.roche.interview.sharedkernel.money.application;

import com.roche.interview.sharedkernel.money.Price;
import com.roche.interview.sharedkernel.money.application.dto.PriceDto;
import org.mapstruct.Mapper;

import java.util.Currency;

@Mapper
public interface PriceMapper {
    default PriceDto toDto(Price price) {
        return new PriceDto(price.getAmount(), price.getCurrency().getCurrencyCode());
    }

    default Price toEntity(PriceDto price) {
        return new Price(price.getAmount(), Currency.getInstance(price.getCurrency()));
    }
}
