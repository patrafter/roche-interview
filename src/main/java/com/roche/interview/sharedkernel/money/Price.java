package com.roche.interview.sharedkernel.money;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Currency;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Price {
    @Min(0)
    @NotNull
    @Column(nullable = false, name = "price")
    private BigDecimal amount;

    @NotNull
    @Column(nullable = false)
    private Currency currency;

    public Price multiply(BigDecimal multiplier) {
        return amount != null ? new Price(amount.multiply(multiplier), currency) : null;
    }

    public Price add(Price other) {
        Preconditions.checkState(currency != null && amount != null, "Cannot add price when currency or amount is not set");
        Preconditions.checkNotNull(other, "Other price cannot be null");
        Preconditions.checkArgument(currency.equals(other.currency), String.format("Cannot add price of different currencies: %s vs %s", currency, other.currency));
        BigDecimal otherAmount = other.amount != null ? other.amount : BigDecimal.ZERO;
        return new Price(amount.add(otherAmount), currency);
    }
}
