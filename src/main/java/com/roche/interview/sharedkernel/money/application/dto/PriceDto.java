package com.roche.interview.sharedkernel.money.application.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceDto {
    @NotNull
    @Min(0)
    private BigDecimal amount;
    @NotBlank
    private String currency;

    @JsonGetter("amount")
    public BigDecimal getFormattedAmount() {
        return amount.setScale(2);
    }
}
