package com.roche.interview.orders.domain;

import lombok.AllArgsConstructor;

import javax.persistence.EntityNotFoundException;

@AllArgsConstructor
public class LineItemFactory {
    private final ProductRepository productRepository;

    public LineItem create(Long productId, Integer quantity) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new EntityNotFoundException("Product not found, id=" + productId));
        return LineItem.ofProduct(product, quantity);
    }
}
