package com.roche.interview.orders.domain;

import com.roche.interview.sharedkernel.money.Price;
import lombok.*;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "ipr_product", indexes = @Index(name = "SKU_IDX", columnList = "sku", unique = true))
@Where(clause = "deleted != true")
@SQLDelete(sql = "UPDATE ipr_product SET deleted = TRUE WHERE id = ?1", check = ResultCheckStyle.COUNT)
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = PROTECTED)
@NoArgsConstructor
public class Product {
    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String sku;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @Valid
    @NotNull
    @Embedded
    private Price price;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime createdDatetime = LocalDateTime.now();

    @Setter(PRIVATE)
    private Boolean deleted = false;

    public static Product create(@NotBlank String sku, @NotBlank String name, @NotNull Price price) {
        return new Product(null, sku, name, price, LocalDateTime.now(), false);
    }
}
