package com.roche.interview.orders.domain;

import com.roche.interview.sharedkernel.money.Price;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toUnmodifiableList;
import static javax.persistence.CascadeType.ALL;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

@Entity
@Table(name = "ior_order", indexes = @Index(name = "DATE_IDX", columnList = "datetime"))
@Where(clause = "deleted != true")
@SQLDelete(sql = "UPDATE ior_order SET deleted = TRUE WHERE id = ?1", check = ResultCheckStyle.COUNT)
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Order {
    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @Column(nullable = false)
    private UUID orderId = UUID.randomUUID();


    @NotEmpty
    @OneToMany(mappedBy = "order", cascade = ALL)
    private List<LineItem> lineItems;

    @Email
    @NotNull
    @Column(nullable = false)
    private String buyerEmail;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime datetime = LocalDateTime.now();

    @Setter(PRIVATE)
    private Boolean deleted = false;

    public Price getTotalPrice() {
        return emptyIfNull(lineItems)
                .stream()
                .map(LineItem::getPrice)
                .reduce(Price::add)
                .orElse(new Price(null, null));
    }

    public void addLineItems(List<LineItem> newLineItems) {
        this.lineItems = Stream.concat(
                emptyIfNull(this.lineItems).stream(),
                emptyIfNull(newLineItems).stream().peek(li -> li.setOrder(this))
        ).collect(toUnmodifiableList());
    }
}
