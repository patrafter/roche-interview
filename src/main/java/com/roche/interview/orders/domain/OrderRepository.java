package com.roche.interview.orders.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface OrderRepository {

    Order save(Order order);

    Optional<Order> findById(Long id);

    List<Order> findByDatetimeRange(LocalDateTime from, LocalDateTime to);
}
