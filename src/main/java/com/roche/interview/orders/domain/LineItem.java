package com.roche.interview.orders.domain;

import com.roche.interview.sharedkernel.money.Price;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static lombok.AccessLevel.PROTECTED;

@Entity
@Table(name = "ior_lineitem")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(of = {"product", "quantity"})
@AllArgsConstructor(access = PROTECTED)
@NoArgsConstructor
public class LineItem {

    @EmbeddedId
    private LineItemKey id;

    @ManyToOne
    @MapsId("orderId")
    private Order order;

    @ManyToOne
    @MapsId("productId")
    private Product product;

    @Min(1)
    @NotNull
    @Column(nullable = false)
    private Integer quantity;

    public static LineItem ofProduct(Product product, int quantity) {
        return new LineItem(new LineItemKey(null, product.getId()), null, product, quantity);
    }

    public Price getPrice() {
        return product != null ? product.getPrice().multiply(BigDecimal.valueOf(quantity)) : new Price();
    }
}
