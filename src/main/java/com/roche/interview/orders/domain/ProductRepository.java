package com.roche.interview.orders.domain;

import java.util.Optional;

public interface ProductRepository {

    Optional<Product> findById(Long id);

    Iterable<Product> findAll();

    Product save(Product product);

    void delete(Product product);
}
