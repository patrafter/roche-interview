package com.roche.interview.orders.infrastructure;

import com.roche.interview.orders.domain.Order;
import com.roche.interview.orders.domain.OrderRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderJpaRepository extends OrderRepository, JpaRepository<Order, Long> {

    @Override
    @Query("FROM Order o WHERE o.datetime >= ?1 AND o.datetime <= ?2 ORDER BY o.datetime DESC")
    List<Order> findByDatetimeRange(LocalDateTime from, LocalDateTime to);
}
