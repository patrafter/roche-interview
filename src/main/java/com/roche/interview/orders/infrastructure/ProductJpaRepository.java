package com.roche.interview.orders.infrastructure;

import com.roche.interview.orders.domain.Product;
import com.roche.interview.orders.domain.ProductRepository;
import org.springframework.data.repository.CrudRepository;

public interface ProductJpaRepository extends ProductRepository, CrudRepository<Product, Long> {
}
