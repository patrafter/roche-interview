package com.roche.interview.orders.application;

import com.roche.interview.orders.application.dto.ProductDto;
import com.roche.interview.orders.domain.Product;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/products")
@AllArgsConstructor
public class ProductController {
    private final ProductService productService;
    private final ProductMapper productMapper;

    @GetMapping
    public List<ProductDto> getAll() {
        return productMapper.toDto(productService.findAll());
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Valid ProductDto productRequest) {
        Product newProduct = productService.save(productMapper.toEntity(productRequest));
        return ResponseEntity.ok().build();
    }

    @PostMapping("{id}")
    public ResponseEntity<Void> update(@PathVariable Integer id, @RequestBody @Valid ProductDto productRequest) {
        productService.update(id, productMapper.toEntity(productRequest));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        productService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
