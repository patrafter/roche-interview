package com.roche.interview.orders.application.dto;

import com.roche.interview.sharedkernel.money.application.dto.PriceDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private UUID orderId;
    private String buyerEmail;
    private List<LineItemDto> lineItems;
    private PriceDto totalPrice;
}
