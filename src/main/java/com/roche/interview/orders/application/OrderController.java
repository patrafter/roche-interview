package com.roche.interview.orders.application;

import com.roche.interview.orders.application.dto.OrderDto;
import com.roche.interview.orders.application.dto.OrderRequestDto;
import com.roche.interview.orders.domain.Order;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

import static com.google.common.base.MoreObjects.firstNonNull;

@RestController
@RequestMapping("/api/orders")
@AllArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final OrderMapper orderMapper;

    @GetMapping("search")
    public List<OrderDto> searchOrders(@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime from,
                                       @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime to) {
        return orderMapper.toDto(orderService.searchOrders(
                firstNonNull(from, LocalDateTime.now()),
                firstNonNull(to, LocalDateTime.now())
        ));
    }

    @PostMapping
    public ResponseEntity<Void> placeOrder(@RequestBody @Valid OrderRequestDto orderRequest) {
        orderService.placeOrder(orderRequest.getBuyerEmail(), orderMapper.toLineItemsEntities(orderRequest.getLineItems()));
        return ResponseEntity.ok().build();
    }
}
