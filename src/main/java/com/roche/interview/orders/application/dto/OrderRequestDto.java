package com.roche.interview.orders.application.dto;

import com.roche.interview.sharedkernel.money.application.dto.PriceDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequestDto {
    @NotNull
    @Email
    private String buyerEmail;
    @NotEmpty
    @Valid
    private List<LineItemRequestDto> lineItems;
}
