package com.roche.interview.orders.application.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LineItemRequestDto {
    @NotNull
    private Integer quantity;
    @NotNull
    private Long productId;
}
