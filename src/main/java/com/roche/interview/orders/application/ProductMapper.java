package com.roche.interview.orders.application;

import com.roche.interview.orders.application.dto.ProductDto;
import com.roche.interview.orders.domain.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    List<ProductDto> toDto(Iterable<Product> products);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdDatetime", ignore = true)
    @Mapping(target = "price.add", ignore = true)
    @Mapping(target = "price.multiply", ignore = true)
    Product toEntity(ProductDto dto);
}
