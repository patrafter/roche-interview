package com.roche.interview.orders.application;

import com.roche.interview.orders.domain.Product;
import com.roche.interview.orders.domain.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@Slf4j
@AllArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public Product save(Product product) {
        log.info("Saving product {}", product);
        return productRepository.save(product);
    }

    public void update(long id, Product updated) {
        Product productToUpdate = productRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Product with does not exist, id=" + id));
        productToUpdate.setName(updated.getName());
        productToUpdate.setSku(updated.getSku());
        productToUpdate.setPrice(updated.getPrice());
        save(productToUpdate);
    }

    public void deleteById(Long id) {
        productRepository.findById(id).ifPresent(this::delete);
    }

    private void delete(Product product) {
        log.info("Deleting product {}", product);
        productRepository.delete(product);
    }
}
