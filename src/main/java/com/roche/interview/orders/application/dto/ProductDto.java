package com.roche.interview.orders.application.dto;

import com.roche.interview.sharedkernel.money.application.dto.PriceDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    @NotBlank
    private String sku, name;
    @NotNull
    @Valid
    private PriceDto price;
}
