package com.roche.interview.orders.application;

import com.roche.interview.orders.domain.LineItem;
import com.roche.interview.orders.domain.Order;
import com.roche.interview.orders.domain.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

@Service
@Slf4j
@AllArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public Order placeOrder(String buyerEmail, List<LineItem> lineItems) {
        Order order = new Order();
        order.setBuyerEmail(buyerEmail);
        order.addLineItems(lineItems);
        Order savedOrder = orderRepository.save(order);
        log.info("Placed order {}", savedOrder);
        return savedOrder;
    }

    public List<Order> searchOrders(LocalDateTime from, LocalDateTime to) {
        return orderRepository.findByDatetimeRange(from, to);
    }
}
