package com.roche.interview.orders.application;

import com.roche.interview.orders.application.dto.LineItemDto;
import com.roche.interview.orders.domain.LineItem;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface LineItemMapper {

    LineItemDto toDto(LineItem lineItem);

    List<LineItemDto> toDto(List<LineItem> lineItems);
}
