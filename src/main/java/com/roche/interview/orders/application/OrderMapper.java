package com.roche.interview.orders.application;

import com.roche.interview.orders.application.dto.LineItemRequestDto;
import com.roche.interview.orders.application.dto.OrderDto;
import com.roche.interview.orders.domain.LineItem;
import com.roche.interview.orders.domain.LineItemFactory;
import com.roche.interview.orders.domain.Order;
import com.roche.interview.orders.domain.ProductRepository;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;

@Mapper(componentModel = "spring", uses = ProductMapper.class)
public abstract class OrderMapper {
    @Autowired
    ProductRepository productRepository;

    public abstract List<OrderDto> toDto(List<Order> orders);

    public List<LineItem> toLineItemsEntities(List<LineItemRequestDto> lineItemRequestDtos) {
        LineItemFactory lineItemFactory = new LineItemFactory(productRepository);
        return emptyIfNull(lineItemRequestDtos)
                .stream()
                .map(dto -> lineItemFactory.create(dto.getProductId(), dto.getQuantity()))
                .collect(toList());
    }
}
